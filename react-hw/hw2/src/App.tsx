import "./App.scss";
import Header from "./components/Header/Header";
import ProductCatalog from "./components/ProductCatalog/ProductCatalog";
import { useEffect, useState } from "react";

function App() {
  const [likedProducts, setLikedProducts] = useState(0);
  const [addedProducts, setAddedProducts] = useState(0);

  useEffect(() => {
    const addedCount = localStorage.getItem("addedCount");
    const likedCount = localStorage.getItem("likedCount");
    addedCount
      ? setAddedProducts(+addedCount)
      : localStorage.setItem("addedCount", "0");

    likedCount
      ? setLikedProducts(+likedCount)
      : localStorage.setItem("likedCount", "0");
  }, []);

  return (
    <>
      <Header likedProducts={likedProducts} addedProducts={addedProducts} />
      <ProductCatalog
        setLikedProducts={setLikedProducts}
        setAddedProducts={setAddedProducts}
      />
    </>
  );
}

export default App;

import { useState, useEffect } from "react";
import Product from "../../types/product";
import Button from "../Button/Button";
import styles from "./ProductCard.module.scss";
import ProductModal from "../Modal/ProductModal";

interface ProductCardProps {
  product: Product;
  setAddedProducts: (arg: number) => void;
  setLikedProducts: (arg: number) => void;
}

function ProductCard({
  product,
  setAddedProducts,
  setLikedProducts,
}: ProductCardProps) {
  const [isModal, setIsModal] = useState(false);
  const [isLiked, setIsLiked] = useState(false);
  const [isAdded, setIsAdded] = useState(false);

  const toggleModal = () => {
    setIsModal(!isModal);
  };

  useEffect(() => {
    if (localStorage.getItem(`${product.vendorCode}_liked`)) {
      setIsLiked(true);
    }
    if (localStorage.getItem(`${product.vendorCode}_added`)) {
      setIsAdded(true);
    }
  }, []);

  const handleAddToFavorites = () => {
    if (!isLiked) {
      localStorage.setItem(
        "likedCount",
        `${+localStorage.getItem("likedCount")! + 1}`
      );
      localStorage.setItem(`${product.vendorCode}_liked`, "true");
    } else {
      localStorage.setItem(
        "likedCount",
        `${+localStorage.getItem("likedCount")! - 1}`
      );
      localStorage.removeItem(`${product.vendorCode}_liked`);
    }

    setLikedProducts(+localStorage.getItem("likedCount")!);
    setIsLiked(!isLiked);
  };

  const handleAddToCart = () => {
    if (!isAdded) {
      localStorage.setItem(
        "addedCount",
        `${+localStorage.getItem("addedCount")! + 1}`
      );
      localStorage.setItem(`${product.vendorCode}_added`, "true");
    } else {
      localStorage.setItem(
        "addedCount",
        `${+localStorage.getItem("addedCount")! - 1}`
      );
      localStorage.removeItem(`${product.vendorCode}_added`);
    }

    setAddedProducts(+localStorage.getItem("addedCount")!);
    setIsAdded(!isAdded);
  };

  return (
    <>
      <div className={styles["product-card"]}>
        <img
          className={styles["product-card__image"]}
          src={product.pictureURL}
          alt={`${product.name} icon`}
        />
        <div
          className={styles["product-card__add-to-favorites"]}
          onClick={handleAddToFavorites}
        >
          <svg
            className={styles["product-card__add-to-favorites-heart"]}
            width="30px"
            height="30px"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
            style={{ fill: isLiked ? "#f3b664" : "none" }}
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M12 6.00019C10.2006 3.90317 7.19377 3.2551 4.93923 5.17534C2.68468 7.09558 2.36727 10.3061 4.13778 12.5772C5.60984 14.4654 10.0648 18.4479 11.5249 19.7369C11.6882 19.8811 11.7699 19.9532 11.8652 19.9815C11.9483 20.0062 12.0393 20.0062 12.1225 19.9815C12.2178 19.9532 12.2994 19.8811 12.4628 19.7369C13.9229 18.4479 18.3778 14.4654 19.8499 12.5772C21.6204 10.3061 21.3417 7.07538 19.0484 5.17534C16.7551 3.2753 13.7994 3.90317 12 6.00019Z"
              stroke="#F3B664"
              strokeWidth="3"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
        </div>
        <div className={styles["product-card__description"]}>
          <div className={styles["product-card__description-text"]}>
            <h2 className={styles["product-card__name"]} title={product.name}>
              {product.name}
            </h2>
            <p className={styles["product-card__vendor"]}>
              {product.vendorCode}
            </p>
          </div>
          <p className={styles["product-card__price"]}>{`$${product.price}`}</p>
        </div>
        {!isAdded ? (
          <Button text="Add to card" onClick={toggleModal} />
        ) : (
          <Button
            text="Remove"
            onClick={handleAddToCart}
            backgroundColor="#FA7070"
          />
        )}
      </div>
      {isModal && (
        <ProductModal
          closeButton
          title="Add to cart?"
          text={`Add ${product.name} to your cart?`}
          onClose={toggleModal}
          addToCart={handleAddToCart}
        />
      )}
    </>
  );
}

export default ProductCard;

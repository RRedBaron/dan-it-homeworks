import { useEffect, useState } from "react";
import Product from "../../types/product";
import ProductCard from "../ProductCard/ProductCard";
import styles from "./ProductCatalog.module.scss";

interface ProductCatalogProps {
  setAddedProducts: (arg: number) => void;
  setLikedProducts: (arg: number) => void;
}

function ProductCatalog({
  setLikedProducts,
  setAddedProducts,
}: ProductCatalogProps) {
  const [products, setProducts] = useState<Product[]>([]);

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const respone = await fetch("/products.json");
        const data = await respone.json();
        setProducts(data);
      } catch (error) {
        console.log(error);
      }
    };

    fetchProducts();
  }, []);

  return (
    <div className={styles["product-catalog"]}>
      {products.map((product) => {
        return (
          <ProductCard
            product={product}
            setLikedProducts={setLikedProducts}
            setAddedProducts={setAddedProducts}
            key={product.vendorCode}
          />
        );
      })}
    </div>
  );
}

export default ProductCatalog;

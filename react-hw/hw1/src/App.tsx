import { useState } from "react";
import "./App.scss";
import Button from "./components/Button/Button.tsx";
import Modal from "./components/Modal/Modal.tsx";
import React from "react";

function App() {
  const [showFirstModal, setShowFirstModal] = useState(false);
  const [showSecondModal, setShowSecondModal] = useState(false);

  const toggleFirstModal = () => {
    setShowFirstModal(!showFirstModal);
  };

  const toggleSecondModal = () => {
    setShowSecondModal(!showSecondModal);
  };

  return (
    <>
      <div className="wrapper">
        <Button
          backgroundColor="#6488ea"
          text="Open first modal"
          onClick={toggleFirstModal}
        ></Button>
        <Button
          backgroundColor="#ec644b"
          text="Open second modal"
          onClick={toggleSecondModal}
        ></Button>
      </div>
      {showFirstModal && (
        <Modal
          title="Modal1"
          text="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dicta
          mollitia error commodi omnis veritatis?"
          actions={
            <React.Fragment>
              <Button
                backgroundColor="#F4E98C"
                text="Action one"
                onClick={() => prompt("Action one")}
              />
              <Button
                backgroundColor="#66A586"
                text="Action two"
                onClick={() => confirm("Action two")}
              />
            </React.Fragment>
          }
          onClose={toggleFirstModal}
        />
      )}
      {showSecondModal && (
        <Modal
          title="Modal2"
          closeButton
          text="Veniam obcaecati sunt et ipsam laborum magni delectus, tenetur voluptate, vel, debitis quod corporis laboriosam possimus!"
          actions={
            <Button
              backgroundColor="#5E0A80"
              text="Action three"
              onClick={() => alert("Action three")}
            ></Button>
          }
          onClose={toggleSecondModal}
        />
      )}
    </>
  );
}

export default App;

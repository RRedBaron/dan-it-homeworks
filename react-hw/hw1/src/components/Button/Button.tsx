import styles from "./Button.module.scss";

interface ButtonProps {
  backgroundColor: string;
  text: string;
  onClick: () => void;
}

function Button({ backgroundColor, text, onClick }: ButtonProps) {
  return (
    <button
      className={styles.button}
      onClick={onClick}
      style={{ backgroundColor }}
    >
      {text}
    </button>
  );
}

export default Button;

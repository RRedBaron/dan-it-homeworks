import styles from "./Modal.module.scss";

interface ModalProps {
  title: string;
  closeButton?: boolean;
  text: string;
  actions: JSX.Element;
  onClose: () => void;
}

function Modal({
  title,
  closeButton = false,
  text,
  actions,
  onClose,
}: ModalProps) {
  return (
    <div className={styles.modal} onClick={onClose}>
      <div
        className={styles["modal__content"]}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <div className={styles["modal__header"]}>
          <p className={styles["modal__title"]}>{title}</p>
          {closeButton && (
            <button className={styles["modal__close-button"]} onClick={onClose}>
              X
            </button>
          )}
        </div>
        <div className={styles["modal__text"]}>{text}</div>
        <div className={styles["modal__actions"]}>{actions}</div>
      </div>
    </div>
  );
}

export default Modal;

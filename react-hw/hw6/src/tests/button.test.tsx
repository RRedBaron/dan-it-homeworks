import React from "react";
import Button from "../components/Button";
import { render, fireEvent } from '@testing-library/react';

describe('Button', () => {

    test('should render button', () => {
        const component = <Button text="Click me" onClick={() => {}} />;
        expect(component).toMatchSnapshot();
    });

    test('should handle click', () => {
        const mockOnClick = jest.fn();
        const component = <Button text="Click me" onClick={mockOnClick} />;
        const { getByText } = render(component);
        const button = getByText('Click me');
        fireEvent.click(button);
        expect(mockOnClick).toHaveBeenCalled();
    });
})

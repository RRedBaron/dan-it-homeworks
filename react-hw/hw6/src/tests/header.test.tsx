import { render, screen } from '@testing-library/react';
import Header from "../components/Header";
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { configureStore } from '@reduxjs/toolkit';
import rootReducer from "../redux/rootReducer";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from 'react-router-dom';

// Update the Product type for testing
type Product = {
    id: number;
    name: string;
    price: number;
    pictureURL: string;
    vendorCode: string;
    color: string;
};

describe('Header Component', () => {
    test('Renders header component', () => {
        const store = configureStore({
            reducer: rootReducer,
        });

        render(
            <Provider store={store}>
                <Router>
                    <Header />
                </Router>
            </Provider>
        );

        const logoElement = screen.getByTestId('header-logo');
        expect(logoElement).toBeInTheDocument();
    });

    test('Displays liked count in header menu', () => {
        const likedProduct: Product = {
            id: 1,
            name: "Product 1",
            price: 19.99,
            pictureURL: "/images/product1.jpg",
            vendorCode: "ABC123",
            color: "Blue",
        };

        const store = configureStore({
            reducer: rootReducer,
            preloadedState: {
                products: {
                    products: [likedProduct],
                    likedProducts: [likedProduct],
                    addedProducts: [],
                },
            },
        });

        render(
            <Provider store={store}>
                <Router>
                    <Header />
                </Router>
            </Provider>
        );

        const likedCountElement = screen.getByTestId('liked-count');
        expect(likedCountElement).toHaveTextContent('1');
    });

    test('Displays added count in header menu', () => {
        const addedProduct: Product = {
            id: 1,
            name: "Product 1",
            price: 19.99,
            pictureURL: "/images/product1.jpg",
            vendorCode: "ABC123",
            color: "Blue",
        };

        const store = configureStore({
            reducer: rootReducer,
            preloadedState: {
                products: {
                    products: [addedProduct],
                    likedProducts: [],
                    addedProducts: [addedProduct],
                },
            },
        });

        render(
            <Provider store={store}>
                <Router>
                    <Header />
                </Router>
            </Provider>
        );

        const addedCountElement = screen.getByTestId('added-count');
        expect(addedCountElement).toHaveTextContent('1');
    });

});

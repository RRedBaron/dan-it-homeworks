import {EnhancedStore, configureStore} from '@reduxjs/toolkit';
import productSlice, {
    fetchProducts,
    fetchLikedProducts,
    removeLikedProduct,
    fetchAddedProducts,
    removeAddedProduct,
    removeAddedProducts,
} from '../redux/productSlice';

describe('productSlice async actions', () => {
    let store: EnhancedStore;
    beforeEach(() => {
        store = configureStore({
            reducer: {
                product: productSlice,
            },
            preloadedState: {
                product: {
                    products: [],
                    likedProducts: [],
                    addedProducts: [],
                },
            },
        });
    });

    test('fetchProducts should handle successful API call', async () => {
        store.dispatch(fetchProducts() as never);
        const state = store.getState().product;
        expect(state.products.length).toBeGreaterThanOrEqual(0);
    });

    test('fetchLikedProducts should handle successful local storage fetch', async () => {
        store.dispatch(fetchLikedProducts() as never);
        const state = store.getState().product;
        expect(state.likedProducts.length).toBeGreaterThanOrEqual(0);
    });

    test('removeLikedProduct should remove a product from likedProducts in local storage', async () => {
        const productToRemove = {
            name: 'Test Product',
            price: 10,
            pictureURL: 'test.jpg',
            vendorCode: 'ABC123',
            color: 'Red',
        };
        store.dispatch(removeLikedProduct(productToRemove) as never);
        const state = store.getState().product;
        expect(state.likedProducts).not.toContainEqual(productToRemove);
    });

    test('fetchAddedProducts should handle successful local storage fetch', async () => {
        store.dispatch(fetchAddedProducts() as never);
        const state = store.getState().product;
        expect(state.addedProducts.length).toBeGreaterThanOrEqual(0);
    });

    test('removeAddedProduct should remove a product from addedProducts in local storage', async () => {
        const productToRemove = {
            name: 'Test Product',
            price: 10,
            pictureURL: 'test.jpg',
            vendorCode: 'ABC123',
            color: 'Red',
        };
        store.dispatch(removeAddedProduct(productToRemove) as never);
        const state = store.getState().product;
        expect(state.addedProducts).not.toContainEqual(productToRemove);
    });

    test('removeAddedProducts should clear addedProducts in local storage', async () => {
        store.dispatch(removeAddedProducts() as never);
        const state = store.getState().product;
        expect(state.addedProducts.length).toBe(0);
    });
});

import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import ProductModal from "../components/ProductModal";

// Mock functions for onClose and addToCart
const mockOnClose = jest.fn();
const mockAddToCart = jest.fn();

describe("ProductModal", () => {
    test("renders modal with title and text", () => {
        render(
            <ProductModal
                title="Test Title"
                text="Test Text"
                onClose={mockOnClose}
                addToCart={mockAddToCart}
            />
        );

        expect(screen.getByText("Test Title")).toBeInTheDocument();
        expect(screen.getByText("Test Text")).toBeInTheDocument();
    });

    test("renders close button when closeButton prop is true", () => {
        render(
            <ProductModal
                title="Test Title"
                text="Test Text"
                onClose={mockOnClose}
                addToCart={mockAddToCart}
                closeButton
            />
        );

        expect(screen.getByText("X")).toBeInTheDocument();
    });

    test("calls onClose when clicking on modal background", () => {
        render(
            <ProductModal
                title="Test Title"
                text="Test Text"
                onClose={mockOnClose}
                addToCart={mockAddToCart}
            />
        );

        fireEvent.click(screen.getByTestId("modal-background"));
        expect(mockOnClose).toHaveBeenCalled();
    });

    test("calls onClose and addToCart when Confirm button is clicked", () => {
        render(
            <ProductModal
                title="Test Title"
                text="Test Text"
                onClose={mockOnClose}
                addToCart={mockAddToCart}
            />
        );

        fireEvent.click(screen.getByText("Confirm"));
        expect(mockOnClose).toHaveBeenCalled();
        expect(mockAddToCart).toHaveBeenCalled();
    });

    test("calls onClose when Cancel button is clicked", () => {
        render(
            <ProductModal
                title="Test Title"
                text="Test Text"
                onClose={mockOnClose}
                addToCart={mockAddToCart}
            />
        );

        fireEvent.click(screen.getByText("Cancel"));
        expect(mockOnClose).toHaveBeenCalled();
    });

    test("does not render close button when closeButton prop is false", () => {
        render(
            <ProductModal
                title="Test Title"
                text="Test Text"
                onClose={mockOnClose}
                addToCart={mockAddToCart}
                closeButton={false}
            />
        );

        expect(screen.queryByText("X")).not.toBeInTheDocument();
    });
});

import { combineReducers } from "@reduxjs/toolkit";
import productsSlice from "./productSlice";
import modalSlice from "./modalSlice";

const rootReducer = combineReducers({
  products: productsSlice,
  modal: modalSlice,
});

export default rootReducer;

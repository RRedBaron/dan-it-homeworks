import React from "react";
import {RootState, useAppSelector} from "../redux/store";
import styles from "./cmpStyles/ProductCatalog.module.scss";
import {Product} from "../types";
import ProductCard from "./ProductCard";
import {useViewMode} from "./ViewModeProvider";

function LikedProductCatalog() {
    const { likedProducts: products } = useAppSelector((state: RootState) => state.products);
    const { viewMode } = useViewMode();

    return (
        <>
            {products.length > 0 ? (
                <div className={styles["product-catalog"]} style={{'display': viewMode === "table" ? "grid" : "flex"}} >
                    {products.map((product: Product) => (
                        <ProductCard product={product}/>
                    ))}
                </div>
            ) : (
                <h3 className="not-found-msg">Товари не знайдені :(</h3>
            )}
        </>
    );
}

export default LikedProductCatalog;
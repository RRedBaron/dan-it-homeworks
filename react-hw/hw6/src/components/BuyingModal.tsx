import styles from "./cmpStyles/BuyingModal.module.scss";
import {Formik, Form, Field, ErrorMessage} from "formik";
import {basicSchema} from "../schemas";
import {useAppDispatch, useAppSelector} from "../redux/store";
import {toggleModal} from "../redux/modalSlice";
import React from "react";
import {removeAddedProducts} from "../redux/productSlice";
import {PatternFormat} from "react-number-format";

interface IFormikValues {
    name: string;
    surname: string;
    address: string;
    phone: string;
    age: string;
}

interface IPhoneInputProps {
    field: any;
    form: any;
}

const PhoneInput = ({field, form, ...props}: IPhoneInputProps) => {
    const handleValueChange = (values: any) => {
        form.setFieldValue(field.name, values.value);
    };

    return (
        <PatternFormat
            {...field}
            {...props}
            mask="_"
            allowEmptyFormatting
            format="+(380) ##-###-##-##"
            onValueChange={handleValueChange}
        />
    );
};

function BuyingModal() {
    const addedProducts = useAppSelector(state => state.products.addedProducts);
    const dispatch = useAppDispatch();

    const initialValues: IFormikValues = {
        name: "",
        surname: "",
        address: "",
        phone: "",
        age: "",
    }

    const handleConfirmPurchase = (values: IFormikValues) => {
        dispatch(removeAddedProducts());
        dispatch(toggleModal());
    }

    const handleModalClick = (e: React.MouseEvent<HTMLDivElement>) => {
        // Check if the click target is the modal content or one of its descendants
        if (e.target === e.currentTarget) {
            // If so, close the modal
            dispatch(toggleModal());
        }
    };

    return (
        <div className={styles.modal} onClick={handleModalClick}>
            <div className={styles["modal__content"]} onClick={(e) => {
                e.stopPropagation()
            }}>
                <div className={styles["modal__header"]}>
                    <p className={styles["modal__title"]}>Confirm purchase</p>
                    <button className={styles["modal__close-button"]} onClick={() => dispatch(toggleModal())}>
                        X
                    </button>
                </div>
                <Formik initialValues={initialValues} onSubmit={handleConfirmPurchase} validationSchema={basicSchema}>
                    <Form className={styles["modal__form"]}>
                        <div className={styles["input__wrapper"]}>
                            <label htmlFor={"name"} className={styles["label"]}>Name</label>
                            <Field id="nameInput" className={styles["input"]} name="name" placeholder="Name"
                                   type="text"/>
                            <ErrorMessage name="name" component="div" className={styles["error"]}/>
                        </div>
                        <div className={styles["input__wrapper"]}>
                            <label htmlFor={"surname"} className={styles["label"]}>Surname</label>
                            <Field id="surnameInput" className={styles["input"]} name="surname" placeholder="Surname"
                                   type="text"/>
                            <ErrorMessage name="surname" component="div" className={styles["error"]}/>
                        </div>
                        <div className={styles["input__wrapper"]}>
                            <label htmlFor={"age"} className={styles["label"]}>Age</label>
                            <Field id="ageInput" className={styles["input"]} name="age" placeholder="Age"
                                   type="number"/>
                            <ErrorMessage name="age" component="div" className={styles["error"]}/>
                        </div>
                        <div className={styles["input__wrapper"]}>
                            <label htmlFor={"address"} className={styles["label"]}>Address</label>
                            <Field id="addressInput" className={styles["input"]} name="address" placeholder="Address"
                                   type="text"/>
                            <ErrorMessage name="address" component="div" className={styles["error"]}/>
                        </div>
                        <div className={styles["input__wrapper"]}>
                            <label htmlFor={"phone"} className={styles["label"]}>
                                Phone
                            </label>
                            <Field
                                id="phoneInput"
                                className={styles["input"]}
                                name="phone"
                                placeholder="Phone"
                                component={PhoneInput}
                            />
                            <ErrorMessage name="phone" component="div" className={styles["error"]}/>
                        </div>
                        <button className={styles["modal__submit"]} type="submit">Submit</button>
                    </Form>
                </Formik>
            </div>

        </div>
    )
}

export default BuyingModal;
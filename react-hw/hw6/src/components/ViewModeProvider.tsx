import React, { createContext, useContext, useState, ReactNode } from "react";

type ViewMode = "table" | "cards";

interface ViewModeContextProps {
    viewMode: ViewMode;
    toggleViewMode: () => void;
}

const ViewModeContext = createContext<ViewModeContextProps | undefined>(undefined);

interface ViewModeProviderProps {
    children: ReactNode;
}

export function ViewModeProvider({ children }: ViewModeProviderProps): JSX.Element {
    const [viewMode, setViewMode] = useState<ViewMode>("table");

    const toggleViewMode = () => {
        setViewMode((prevMode) => (prevMode === "cards" ? "table" : "cards"));
    };

    return (
        <ViewModeContext.Provider value={{ viewMode, toggleViewMode }}>
            {children}
        </ViewModeContext.Provider>
    );
}

export const useViewMode = (): ViewModeContextProps => {
    const context = useContext(ViewModeContext);
    if (!context) {
        throw new Error("useViewMode must be used within a ViewModeProvider");
    }
    return context;
};

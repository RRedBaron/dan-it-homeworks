import React from "react";
import styles from "./cmpStyles/Button.module.scss";

interface ButtonProps {
    text: string;
    style?: object;
    onClick: () => void;
}

function Button({text, onClick, style = {backgroundColor: "#79ac78"}}: ButtonProps) {
    return (
        <button
            className={styles.button}
            onClick={onClick}
            style={style}
        >
            {text}
        </button>
    );
}

export default Button;

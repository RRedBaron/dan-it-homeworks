import React from "react";
import Button from "./Button";
import styles from "./cmpStyles/ProductModal.module.scss";

interface ProductModalProps {
    title: string;
    closeButton?: boolean;
    text: string;
    onClose: () => void;
    addToCart: () => void;
}

function ProductModal({
                          title,
                          closeButton = false,
                          text,
                          onClose,
                          addToCart,
                      }: ProductModalProps) {
    return (
        <div data-testid="modal-background" className={styles.modal} onClick={onClose}>
            <div
                className={styles["modal__content"]}
                onClick={(e) => {
                    e.stopPropagation();
                }}
            >
                <div className={styles["modal__header"]}>
                    <p className={styles["modal__title"]}>{title}</p>
                    {closeButton && (
                        <button className={styles["modal__close-button"]} onClick={onClose}>
                            X
                        </button>
                    )}
                </div>
                <div className={styles["modal__text"]}>{text}</div>
                <div className={styles["modal__actions"]}>
                    <Button
                        text="Confirm"
                        onClick={() => {
                            addToCart();
                            onClose();
                        }}
                    ></Button>
                    <Button text="Cancel" onClick={onClose} style={{backgroundColor: "#FA7070"}}/>
                </div>
            </div>
        </div>
    );
}

export default ProductModal;

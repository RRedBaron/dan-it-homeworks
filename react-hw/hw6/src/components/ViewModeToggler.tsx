import React from "react";
import styles from "./cmpStyles/ViewModeToggler.module.scss";
import {useViewMode} from "./ViewModeProvider";

function ViewModeToggler() {
    const {viewMode, toggleViewMode} = useViewMode();

    return (
        <label className={styles.toggleSwitch}>
            <input type="checkbox" checked={viewMode === "table"} onChange={toggleViewMode}/>
            <span className={styles.slider}></span>
        </label>
    )
}

export default ViewModeToggler;
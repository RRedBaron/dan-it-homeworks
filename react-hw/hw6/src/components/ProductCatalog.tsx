import React from "react";
import ProductCard from "./ProductCard";
import styles from "./cmpStyles/ProductCatalog.module.scss";
import {RootState, useAppSelector} from "../redux/store";
import {Product} from "../types";
import {useViewMode} from "./ViewModeProvider";

function ProductCatalog() {
    const {products} = useAppSelector((state: RootState) => state.products);
    const {viewMode} = useViewMode();

    return (
        <>
            {products.length > 0 ? (

                <div className={styles["product-catalog"]} style={{'display': viewMode === "table" ? "grid" : "flex"}}>
                    {products.map((product: Product) => (
                        <ProductCard key={product.vendorCode} product={product}/>
                    ))}
                </div>
            ) : (
                <h3 className="not-found-msg">Товари не знайдені :(</h3>
            )}
        </>
    );
}

export default ProductCatalog;

export enum ProductsActionTypes {
  FETCH_PRODUCTS_START = "FETCH_PRODUCTS_START",
  FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS",
  FETCH_PRODUCTS_ERROR = "FETCH_PRODUCTS_ERROR",
}

export type ProductsAction =
  | { type: ProductsActionTypes.FETCH_PRODUCTS_START }
  | { type: ProductsActionTypes.FETCH_PRODUCTS_SUCCESS; payload: any }
  | { type: ProductsActionTypes.FETCH_PRODUCTS_ERROR; payload: any };

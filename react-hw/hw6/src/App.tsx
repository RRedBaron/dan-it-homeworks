import React from "react";
import {Routes, Route} from "react-router-dom";
import Header from "./components/Header";
import ProductCatalog from "./components/ProductCatalog";
import {useEffect} from "react";
import "./App.scss";
import Footer from "./components/Footer";
import {fetchAddedProducts, fetchLikedProducts, fetchProducts} from "./redux/productSlice";
import {useAppDispatch} from "./redux/store";
import LikedProductCatalog from "./components/LikedProductCatalog";
import AddedProductCatalog from "./components/AddedProductCatalog";
import {ViewModeProvider} from "./components/ViewModeProvider";
import ViewModeToggler from "./components/ViewModeToggler";

function App() {
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(fetchProducts());
        dispatch(fetchLikedProducts());
        dispatch(fetchAddedProducts());
    }, [dispatch]);

    return (
        <>

            <ViewModeProvider>
                <Header/>
                <Routes>
                    <Route path="/" element={<ProductCatalog/>}/>
                    <Route path="/liked" element={<LikedProductCatalog/>}/>
                    <Route path="/added" element={<AddedProductCatalog/>}/>
                </Routes>
                <Footer/>
            </ViewModeProvider>
        </>
    );
}

export default App;

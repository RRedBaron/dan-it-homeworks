import {useState} from "react";
import Product from "../types/product.ts";
import Button from "./Button.tsx";
import styles from "./cmpStyles/ProductCard.module.scss";
import ProductModal from "./ProductModal.tsx";
import {RootState, useAppDispatch, useAppSelector} from "../redux/store.ts";
import {addAddedProduct, addLikedProduct, removeAddedProduct, removeLikedProduct} from "../redux/productSlice.ts";


interface ProductCardProps {
    product: Product;
}

function ProductCard({product}: ProductCardProps) {
    const dispatch = useAppDispatch();
    const [isModal, setIsModal] = useState(false);
    const isLiked = useAppSelector((state: RootState) => {
        return state.products.likedProducts.some((likedProduct) => likedProduct.vendorCode === product.vendorCode);
    });
    const isAdded = useAppSelector((state: RootState) => {
        return state.products.addedProducts.some((addedProduct) => addedProduct.vendorCode === product.vendorCode);
    });

    const toggleModal = () => {
        setIsModal(!isModal);
    };

    return (
        <>
            <div className={styles["product-card"]}>
                <img
                    className={styles["product-card__image"]}
                    src={product.pictureURL}
                    alt={`${product.name} icon`}
                />
                <div
                    className={styles["product-card__add-to-favorites"]}
                    onClick={() => {
                        !isLiked ? dispatch(addLikedProduct(product)) : dispatch(removeLikedProduct(product));
                    }}
                >
                    <svg
                        className={styles["product-card__add-to-favorites-heart"]}
                        width="30px"
                        height="30px"
                        viewBox="0 0 24 24"
                        xmlns="http://www.w3.org/2000/svg"
                        style={{fill: isLiked ? "#f3b664" : "none"}}
                    >
                        <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M12 6.00019C10.2006 3.90317 7.19377 3.2551 4.93923 5.17534C2.68468 7.09558 2.36727 10.3061 4.13778 12.5772C5.60984 14.4654 10.0648 18.4479 11.5249 19.7369C11.6882 19.8811 11.7699 19.9532 11.8652 19.9815C11.9483 20.0062 12.0393 20.0062 12.1225 19.9815C12.2178 19.9532 12.2994 19.8811 12.4628 19.7369C13.9229 18.4479 18.3778 14.4654 19.8499 12.5772C21.6204 10.3061 21.3417 7.07538 19.0484 5.17534C16.7551 3.2753 13.7994 3.90317 12 6.00019Z"
                            stroke="#F3B664"
                            strokeWidth="3"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        />
                    </svg>
                </div>
                <div className={styles["product-card__description"]}>
                    <div className={styles["product-card__description-text"]}>
                        <h2 className={styles["product-card__name"]} title={product.name}>
                            {product.name}
                        </h2>
                        <p className={styles["product-card__vendor"]}>
                            {product.vendorCode}
                        </p>
                    </div>
                    <p className={styles["product-card__price"]}>{`$${product.price}`}</p>
                </div>
                {!isAdded ? (
                    <Button text="Add to card" onClick={toggleModal}/>
                ) : (
                    <Button
                        text="Remove"
                        onClick={() => {
                            !isAdded ? dispatch(addAddedProduct(product)) : dispatch(removeAddedProduct(product));
                        }}
                        backgroundColor="#FA7070"
                    />
                )}
            </div>
            {isModal && (
                <ProductModal
                    closeButton
                    title="Add to cart?"
                    text={`Add ${product.name} to your cart?`}
                    onClose={toggleModal}
                    addToCart={() => {
                        dispatch(addAddedProduct(product));
                    }}
                />
            )}
        </>
    );
}

export default ProductCard;

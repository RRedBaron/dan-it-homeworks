import {RootState, useAppSelector} from "../redux/store.ts";
import styles from "./cmpStyles/ProductCatalog.module.scss";
import {Product} from "../types";
import ProductCard from "./ProductCard.tsx";

function LikedProductCatalog() {
    const { likedProducts: products } = useAppSelector((state: RootState) => state.products);

    return (
        <>
            {products.length > 0 ? (
                <div className={styles["product-catalog"]}>
                    {products.map((product: Product) => (
                        <ProductCard product={product}/>
                    ))}
                </div>
            ) : (
                <h3 className="not-found-msg">Товари не знайдені :(</h3>
            )}
        </>
    );
}

export default LikedProductCatalog;
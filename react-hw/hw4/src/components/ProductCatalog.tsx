import ProductCard from "./ProductCard.tsx";
import styles from "./cmpStyles/ProductCatalog.module.scss";
import {RootState, useAppSelector} from "../redux/store.ts";
import {Product} from "../types";

function ProductCatalog() {
  const { products } = useAppSelector((state: RootState) => state.products);

  return (
    <>
      {products.length > 0 ? (
        <div className={styles["product-catalog"]}>
          {products.map((product: Product) => (
            <ProductCard product={product}/>
          ))}
        </div>
      ) : (
        <h3 className="not-found-msg">Товари не знайдені :(</h3>
      )}
    </>
  );
}

export default ProductCatalog;

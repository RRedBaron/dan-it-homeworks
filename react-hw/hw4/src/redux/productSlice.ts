import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import {fetchFromLocalStorage, addToLocalStorage, removeFromLocalStorage} from "../utils/utility";
import {Product} from "../types";

type ProductsState = {
    products: Product[];
    likedProducts: Product[];
    addedProducts: Product[];
};

const initialState: ProductsState = {
    products: [],
    likedProducts: [],
    addedProducts: [],
};

export const fetchProducts = createAsyncThunk(
    "products/fetchProducts",
    async () => {
        try {
            const response = await fetch("/products.json");
            const data: Product[] = await response.json();
            return data;
        } catch (error) {
            console.log("Error while fetching products", error);
            return [];
        }
    }
);

export const fetchLikedProducts = createAsyncThunk(
    "products/fetchLikedProducts",
    async () => {
        return fetchFromLocalStorage("likedProducts");
    }
);

export const addLikedProduct = createAsyncThunk(
    "products/addLikedProduct",
    async (product: Product) => {
        return addToLocalStorage("likedProducts", product);
    }
);

export const removeLikedProduct = createAsyncThunk(
    "products/removeLikedProduct",
    async (product: Product) => {
        return removeFromLocalStorage("likedProducts", product);
    }
);

export const fetchAddedProducts = createAsyncThunk(
    "products/fetchAddedProducts",
    async () => {
        return fetchFromLocalStorage("addedProducts");
    }
);

export const addAddedProduct = createAsyncThunk(
    "products/addAddedProduct",
    async (product: Product) => {
        return addToLocalStorage("addedProducts", product);
    }
);

export const removeAddedProduct = createAsyncThunk(
    "products/removeAddedProduct",
    async (product: Product) => {
        return removeFromLocalStorage("addedProducts", product);
    }
);

const productSlice = createSlice({
    name: "product",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(fetchProducts.fulfilled, (state, action) => {
            state.products = action.payload;
        });
        builder.addCase(fetchLikedProducts.fulfilled, (state, action) => {
            state.likedProducts = action.payload;
        });
        builder.addCase(addLikedProduct.fulfilled, (state, action) => {
            state.likedProducts = action.payload;
        });
        builder.addCase(removeLikedProduct.fulfilled, (state, action) => {
            state.likedProducts = action.payload;
        });
        builder.addCase(fetchAddedProducts.fulfilled, (state, action) => {
            state.addedProducts = action.payload;
        });
        builder.addCase(addAddedProduct.fulfilled, (state, action) => {
            state.addedProducts = action.payload;
        });
        builder.addCase(removeAddedProduct.fulfilled, (state, action) => {
            state.addedProducts = action.payload;
        });
    },
});

export default productSlice.reducer;

import Product from "./product";

interface RootState {
  products: Product[];
  likedProducts: Product[];
  addedProducts: Product[];
}

export type { RootState, Product };

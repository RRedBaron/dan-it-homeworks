import styles from "./Button.module.scss";

interface ButtonProps {
  text: string;
  backgroundColor?: string;
  onClick: () => void;
}

function Button({ text, onClick, backgroundColor = "#79ac78" }: ButtonProps) {
  return (
    <button
      className={styles.button}
      onClick={onClick}
      style={{ backgroundColor }}
    >
      {text}
    </button>
  );
}

export default Button;

import { useEffect, useState } from "react";
import Product from "../../types/product";
import ProductCard from "../ProductCard/ProductCard";
import styles from "./ProductCatalog.module.scss";

interface ProductCatalogProps {
  setAddedProducts: (arg: number) => void;
  setLikedProducts: (arg: number) => void;
  products: Product[];
}

function ProductCatalog({
  setLikedProducts,
  setAddedProducts,
  products,
}: ProductCatalogProps) {
  return (
    <>
      {products.length > 0 ? (
        <div className={styles["product-catalog"]}>
          {products.map((product) => (
            <ProductCard
              product={product}
              setLikedProducts={setLikedProducts}
              setAddedProducts={setAddedProducts}
              key={product.vendorCode}
            />
          ))}
        </div>
      ) : (
        <h3 className="not-found-msg">Товари не знайдені :(</h3>
      )}
    </>
  );
}

export default ProductCatalog;

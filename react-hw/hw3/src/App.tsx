import { Routes, Route } from "react-router-dom";
import Header from "./components/Header/Header";
import ProductCatalog from "./components/ProductCatalog/ProductCatalog";
import { useEffect, useState } from "react";
import "./App.scss";
import Footer from "./components/Footer/Footer";
import Product from "./types/product";

function App() {
  const [likedProducts, setLikedProducts] = useState(0);
  const [addedProducts, setAddedProducts] = useState(0);
  const [products, setProducts] = useState<Product[]>([]);

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const respone = await fetch("/products.json");
        const data = await respone.json();
        setProducts(data);
      } catch (error) {
        console.log(error);
      }
    };

    fetchProducts();

    const addedCount = localStorage.getItem("addedCount");
    const likedCount = localStorage.getItem("likedCount");
    addedCount
      ? setAddedProducts(+addedCount)
      : localStorage.setItem("addedCount", "0");

    likedCount
      ? setLikedProducts(+likedCount)
      : localStorage.setItem("likedCount", "0");
  }, []);

  return (
    <>
      <Header likedProducts={likedProducts} addedProducts={addedProducts} />
      <Routes>
        <Route
          path="/"
          element={
            <ProductCatalog
              setLikedProducts={setLikedProducts}
              setAddedProducts={setAddedProducts}
              products={products}
            />
          }
        ></Route>
        <Route
          path="/liked"
          element={
            <ProductCatalog
              setLikedProducts={setLikedProducts}
              setAddedProducts={setAddedProducts}
              products={products.filter((product) =>
                localStorage.getItem(`${product.vendorCode}_liked`)
              )}
            />
          }
        ></Route>
        <Route
          path="/added"
          element={
            <ProductCatalog
              setLikedProducts={setLikedProducts}
              setAddedProducts={setAddedProducts}
              products={products.filter((product) =>
                localStorage.getItem(`${product.vendorCode}_added`)
              )}
            />
          }
        ></Route>
      </Routes>
      <Footer />
    </>
  );
}

export default App;

import { Routes, Route } from "react-router-dom";
import Header from "./components/Header.tsx";
import ProductCatalog from "./components/ProductCatalog.tsx";
import { useEffect } from "react";
import "./App.scss";
import Footer from "./components/Footer.tsx";
import {fetchAddedProducts, fetchLikedProducts, fetchProducts} from "./redux/productSlice";
import { useAppDispatch } from "./redux/store";
import LikedProductCatalog from "./components/LikedProductCatalog.tsx";
import AddedProductCatalog from "./components/AddedProductCatalog.tsx";

function App() {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchProducts());
    dispatch(fetchLikedProducts());
    dispatch(fetchAddedProducts());
  }, [dispatch]);

  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<ProductCatalog />}></Route>
        <Route path="/liked" element={<LikedProductCatalog />}></Route>
        <Route path="/added" element={<AddedProductCatalog />}></Route>
      </Routes>
      <Footer />
    </>
  );
}

export default App;

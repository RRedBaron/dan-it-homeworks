import {createSlice} from "@reduxjs/toolkit";

type ModalState = {
    isOpen: boolean;
};

const initialState: ModalState = {
    isOpen: false,
};

const modalSlice = createSlice({
    name: "modal",
    initialState,
    reducers: {
        toggleModal: (state) => {
            state.isOpen = !state.isOpen;
        },
    },
});

export const {toggleModal} = modalSlice.actions;
export const selectModal = (state: { modal: ModalState }) => state.modal;
export default modalSlice.reducer;
import { combineReducers } from "redux";
import productsSlice from "./productSlice";
import modalSlice from "./modalSlice.ts";

const rootReducer = combineReducers({
  products: productsSlice,
  modal: modalSlice,
});

export default rootReducer;

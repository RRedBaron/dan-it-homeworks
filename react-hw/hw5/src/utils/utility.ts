import { Product } from "../types";

export const fetchFromLocalStorage = (key: string) => {
    if (localStorage.getItem(key) === null) {
        localStorage.setItem(key, JSON.stringify([]));
    }
    return JSON.parse(localStorage.getItem(key) || "[]");
};

export const addToLocalStorage = (key: string, item: Product) => {
    const items = fetchFromLocalStorage(key);
    items.push(item);
    localStorage.setItem(key, JSON.stringify(items));
    return items;
}

export const removeFromLocalStorage = (key: string, item: Product) => {
    const items = fetchFromLocalStorage(key);
    const newItems = items.filter((i: Product) => i.vendorCode !== item.vendorCode);
    localStorage.setItem(key, JSON.stringify(newItems));
    return newItems;
}

export const clearLocalStorage = (key: string) => {
    localStorage.removeItem(key);
    return [];
};
import {RootState, useAppDispatch, useAppSelector} from "../redux/store.ts";
import styles from "./cmpStyles/ProductCatalog.module.scss";
import {Product} from "../types";
import ProductCard from "./ProductCard.tsx";
import Button from "./Button.tsx";
import {toggleModal} from "../redux/modalSlice.ts";
import BuyingModal from "./BuyingModal.tsx";

function AddedProductCatalog() {
    const {addedProducts: products} = useAppSelector((state: RootState) => state.products);
    const {isOpen} = useAppSelector((state: RootState) => state.modal);
    const dispatch = useAppDispatch();

    const handleConfirmPurchase = () => {
        dispatch(toggleModal());
    };

    return (
        <>

            {products.length > 0 ? (

                <div className={styles["product-catalog"]}>
                    {products.map((product: Product) => (
                        <ProductCard product={product}/>
                    ))}
                </div>

            ) : (
                <h3 className="not-found-msg">Товари не знайдені :(</h3>
            )}
            {products.length > 0 ? (<Button text={"Confirm purchase"} onClick={handleConfirmPurchase}
                                            style={{
                                                width: "auto", display: "block",
                                                marginLeft: "auto", marginRight: "auto"
                                            }}></Button>) : null}
            {isOpen ? <BuyingModal/> : null}
        </>
    );
}

export default AddedProductCatalog;
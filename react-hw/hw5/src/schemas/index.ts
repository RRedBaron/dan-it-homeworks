import * as yup from 'yup';

export const basicSchema = yup.object().shape({
    name: yup.string().required("Required"),
    surname: yup.string().required("Required"),
    age: yup.number().required("Required").positive().integer().min(18, "You must be at least 18 years old").max(85, "You must be at most 65 years old"),
    address: yup.string().required("Required"),
    phone: yup.string().required("Required"),
});
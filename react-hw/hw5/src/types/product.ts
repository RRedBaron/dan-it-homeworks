type Product = {
  name: string;
  price: number;
  pictureURL: string;
  vendorCode: string;
  color: string;
};

export default Product;

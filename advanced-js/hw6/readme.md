# Асинхронність в JavaScript

Асинхронність в JavaScript - це спосіб виконання коду, при якому програма не чекає, поки якась операція завершиться, а замість цього продовжує виконувати інші дії. Це робиться для того, щоб програма не "зависала" і можна було виконувати інші завдання під час очікування завершення певної дії.

Наприклад, під час завантаження якогось об'єму даних з сервера, програма не чекатиме на повне завантаження цих даних і продовжить виконання наступних команд, повернувшись назад, коли завантаження завершиться.
const apiIpifyUrl = 'https://api.ipify.org/?format=json';
const ipApiUrl = 'http://ip-api.com/json/';

const button = document.querySelector('#button');
button.addEventListener('click', clickHandler);

async function getData(url) {
    try {
        const response = await fetch(url);
        try {
            return await response.json();
        } catch (error) {
            return null;
        }
    } catch (error) {
        console.log(error);
        return null;
    }
}

function clickHandler() {
    getData(apiIpifyUrl).then((data) => {
        return getData(ipApiUrl + data.ip);
    }).then((data) => {
        const div = document.createElement('div');
        div.innerHTML = `
            <p>IP: ${data.query}</p>
            <p>Країна: ${data.country}</p>
            <p>Регіон: ${data.regionName}</p>
            <p>Місто: ${data.city}</p>
            <p>Індекс: ${data.zip}</p>
            <p>Координати: ${data.lat} ${data.lon}</p>
        `
        document.body.append(div);
        button.disabled = true;
    });
}
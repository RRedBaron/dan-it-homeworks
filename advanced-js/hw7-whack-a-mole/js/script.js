document.addEventListener('DOMContentLoaded', function() {
    showGreetingModal();
});

const game = {
    'userScore': 0,
    'computerScore': 0,
}

function showGreetingModal() {
    const greetingModal = document.querySelector('#greeting-modal');
    greetingModal.style.display = 'block';
} 

function showGameOverModal() {
    const gameOverModal = document.querySelector("#gameover-modal");
    gameOverModal.style.display = 'flex';
    gameOverModal.querySelector('.modal__subtitle').innerText = game.userScore === 10 ? 'You win!' : 'You lose!';
}

function startGame(interval) {
    const greetingModal = document.querySelector('#greeting-modal');
    greetingModal.style.display = 'none';
    const cells = renderGameField();

    setMoleCell(interval, cells);
}

function setMoleCell(interval, cells) {
    if (game.computerScore === 10 || game.userScore === 10) {
        showGameOverModal();
        return;
    }
    const timeoutId = setTimeout(() => {
        const moleCell = cells.pop();
        moleCell.setMole();
        moleCell.addClickHandler(timeoutId);
        setTimeout(() => {
            if (!moleCell.isClicked) {
                moleCell.setFailed();
                game.computerScore++;
                updateScoreBoard(game.userScore, game.computerScore);
            }
            setMoleCell(interval, cells);
        }, interval);
    }, interval);
}

function renderGameField() {

    const gameField = document.querySelector('.game-field');
    const cells = []
    for (let i = 0; i < 10; i++) {
        const row = document.createElement('tr');
        row.classList.add('row');
        for (let j = 0; j < 10; j++) {
            const cellButton = new CellButton();
            cells.push(cellButton);
            cellButton.render(row);
        }
        gameField.appendChild(row);
    }
    return cells.sort(() => Math.random() - 0.5);
}

function updateScoreBoard(user, computer) {
    const userScoreElement = document.querySelector('#user-score');
    const computerScoreElement = document.querySelector('#computer-score');
    userScoreElement.innerText = user;
    computerScoreElement.innerText = computer;
}

function restartGame() {
    location.reload();
}

class CellButton {

    isMole = false;
    isClicked = false;

    constructor() {
        this.td = document.createElement('td');
        this.td.classList.add('cell');
        const cellButton = document.createElement('button');
        cellButton.classList.add('cell-button');
        this.td.appendChild(cellButton);
    }

    render(selector) {
        selector.appendChild(this.td);
    }

    setMole() {
        this.isMole = true;
        this.td.querySelector('.cell-button').style.background = 'url("img/mole.svg")';
    }

    setFailed() {
        this.isClicked = true;
        this.isMole = false;
        this.td.querySelector('.cell-button').style.background = '#FF6D60';
    }

    addClickHandler(timeoutId) {
        this.td.querySelector('.cell-button').addEventListener('click', () => {
            if (this.isMole && !this.isClicked) {
                this.isClicked = true;
                this.td.querySelector('.cell-button').style.background = '#98D8AA';
                clearTimeout(timeoutId);
                updateScoreBoard(++game.userScore, game.computerScore);
            }
        });
    }
}
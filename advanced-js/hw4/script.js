// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

// const requestURL = 'https://ajax.test-danit.com/api/swapi/films';

const requestURL = 'https://ajax.test-danit.com/api/swapi/films';

function getCharactersInfo(characters) {
    return Promise.all(characters.map((character) => {
      return fetch(character).then(response => response.json()).then(data => data.name)
    }))
}  

function getStarWarsFilms(url) {
    fetch(url)
        .then(response => { return response.json()})
        .then(films => {
            films.forEach(({episodeId, name, openingCrawl, characters}) => {
                const film = document.createElement('div');
                film.innerHTML = `
                <h2>Episode ${episodeId}: ${name}</h2>
                <p>${openingCrawl}</p>
                <div class='spinner-container'>
                    <div class='spinner'></div>
                </div>`;
                document.body.append(film);
                getCharactersInfo(characters).then((characters) => {
                    const spinner = film.querySelector('.spinner-container');
                    spinner.remove();
                    const charactersList = document.createElement('ul');
                    charactersList.innerHTML = characters.map((character) => {
                        return `<li>${character}</li>`
                    }).join('');
                    film.append(charactersList);
                });
            });
        });
}

getStarWarsFilms(requestURL);

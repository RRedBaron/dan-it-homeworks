class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    set name(name) {
        this._name = name;
    }

    set age(age) {
        this._age = age;
    }

    set salary(salary) {
        this._salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }

    get lang() {
        return this._lang;
    }

    set lang(lang) {
        this._lang = lang;
    }
}

const programmerJohn = new Programmer('John', 25, 1000, ['JavaScript', 'GoLang']);
const programmerVasya = new Programmer('Vasya', 28, 2000, ['Python', 'JavaScript']);
const programmerTaras = new Programmer('Taras', 19, 3500, ['Java', 'JavaScript', 'C++']);

console.log(programmerJohn);
console.log(programmerJohn.salary);

console.log(programmerVasya);
console.log(programmerVasya.salary);

console.log(programmerTaras);
console.log(programmerTaras.salary);
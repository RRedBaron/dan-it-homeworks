const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.querySelector("#root")
const ul = document.createElement('ul')

books.forEach((book) => {
    try {
        if (book.author && book.name && book.price) {
            const liElement = document.createElement('li')
            liElement.innerText = `Книга: ${book.name}\nАвтор: ${book.author}\nЦіна: ${book.price}`
            ul.appendChild(liElement)
        }
        else if(!book.author) {
            throw new Error('No info about author! Book cannot be added D:')
        }
        else if(!book.name) {
            throw new Error('No info about name! Book cannot be added D:')
        }
        else if(!book.price) {
            throw new Error('No info about price! Book cannot be added D:')
        }
    } catch (err) {
        console.log(err.message);
    }

})

root.appendChild(ul)

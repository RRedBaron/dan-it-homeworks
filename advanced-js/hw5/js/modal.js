class Modal {
    constructor(formMethod, {title = '', body = '', id= ''} = {}, card = null) {
        this.title = title;
        this.text = body;
        this.id = id;
        this.formMethod = formMethod;
        this.card = card;
        this.div = null;
    }

    render() {
        this.div = document.createElement('div');
        this.div.classList.add('modal-wrapper');
        this.div.innerHTML = `
        <div class="modal-content">
            <button class="modal-close-button">&times</button>
            <form id="form">
                <label for="title">Title</label>
                <input type="text" id="title" value="${this.title}">
                <label for="body">Text</label>
                <textarea id="body" name="body" rows="4" cols="50">${this.text}</textarea>
                <button class="submit-button">Submit</button>
            </form>
        </div>
        `
        this.addCloseListener();
        this.addSubmitListener();
        document.querySelector("#root").append(this.div)
    }

    addCloseListener() {
        const closeButton = this.div.querySelector('.modal-close-button');
        closeButton.addEventListener('click', () => {
            this.delete();
        });
        this.div.addEventListener('mousedown', (event) => {
            if(event.target === this.div) {
                this.delete();
            }
        });
    }

    addSubmitListener() {
        if(this.formMethod === 'POST') {
            this.addPostSubmitListener();
        }
        else {
            this.addPutSubmitListener();
        }
    }

    addPostSubmitListener() {
        const submitButton = this.div.querySelector('.submit-button');
        submitButton.addEventListener('click', (event) => {
            event.preventDefault();
            if (this.div.querySelector('#title').value === '' || this.div.querySelector('#body').value === '') {
                alert('Please fill in all fields');
                return;
            }
            const title = this.div.querySelector('#title').value;
            const body = this.div.querySelector('#body').value;
            fetch('https://ajax.test-danit.com/api/json/posts', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({title, body, userId: 1})
            }).then(response => {
                if(response.ok) {
                    return response.json();
                }
                else {
                    throw new Error('Something went wrong!');
                }
            }).then(post => {
                fetch('https://ajax.test-danit.com/api/json/users/1').then(response => response.json()).then(user => {
                    this.div.remove();
                    const card = new Card(user, post);
                    card.render('.posts-container');
                });
            });
        });
    }

    addPutSubmitListener() {
        const submitButton = this.div.querySelector('.submit-button');
        submitButton.addEventListener('click', (event) => {
            event.preventDefault();
            if (this.div.querySelector('#title').value === '' || this.div.querySelector('#body').value === '') {
                alert('Please fill in all fields');
                return;
            }
            const title = this.div.querySelector('#title').value;
            const body = this.div.querySelector('#body').value;
            fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({title, body, userId: 1})
            }).then(response => {
                if(response.ok) {
                    this.delete();
                    this.card.changePost({title, body});
                }
                else {
                    throw new Error('Something went wrong!');
                }
            });
        });
    }

    delete() {
        this.div.remove();
    }
}
const usersURL = 'https://ajax.test-danit.com/api/json/users';
const postsURL = 'https://ajax.test-danit.com/api/json/posts';

class Card {
    constructor(user, post) {
        this.user = user;
        this.post = post;
        this.div = null;
    }

    render(selector) {
        this.div = document.createElement('div');
        this.div.classList.add('card');
        this.div.innerHTML = `
        <div class="card">
            <div class="card__header">
                <img class="card__profile-pic" src="assets/pic.jpg" alt="profile pic">
                <div class="card__user">
                    <div class="card__user-info">
                        <h2 class="card__user-name">${this.user.name}</h2>
                        <h4 class="card__user-tag">${this.user.username}</h4>                   
                    </div>
                    <h5 class="card__user-email">${this.user.email}</h5>
                </div>
                <div class="card__buttons">
                    <button class="card__edit-button"><img class="card__edit-icon" src="assets/edit.png" alt="edit icon"></button>
                    <button class="card__delete-button">
                        <svg class="card__delete-icon" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"> <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/> <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/> </svg>
                    </button> 
                </div>              
            </div>
            <div class="card__content">
                <h2 class="card__post-title">${this.post.title}</h2>
                <p class="card__post-text">${this.post.body}</p>
            </div> 
        </div>
        `;

        this.addDeleteListener();
        this.addEditListener();
        document.querySelector(selector).prepend(this.div);
    }

    addDeleteListener() {
        const deleteButton = this.div.querySelector('.card__delete-button');
        deleteButton.addEventListener('click', () => {
            fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`,
                {method: 'DELETE'}
            ).then(response => {
                if(response.status === 200) {
                    this.div.remove();
                }
                else {
                    alert('Something went wrong!');
                }
            })
        });
    }

    addEditListener() {
        const editButton = this.div.querySelector('.card__edit-button');
        editButton.addEventListener('click', () => {
            console.log(this.post);
            const modal = new Modal(editButton, this.post, this);
            modal.render();
        });
    }

    changePost({title, body}) {
        this.div.querySelector('.card__post-title').textContent = title;
        this.div.querySelector('.card__post-text').textContent = body;
    }

}

async function getPosts() {
    const users = await fetch(usersURL).then(response =>  response.json()).then(data => data);
    const posts = await fetch(postsURL).then(response =>  response.json()).then(data => data);

    const spinner = document.querySelector('.loading-spinner');
    spinner?.remove();
    

    posts.forEach(post => {
        const user = users.find(user => user.id === post.userId);
        const card = new Card(user, post);
        card.render('.posts-container');
    });
}

const newPostButton = document.querySelector('.new-post-button');
newPostButton.addEventListener('click', () => {
    const modal = new Modal('POST');
    modal.render();
});
getPosts();